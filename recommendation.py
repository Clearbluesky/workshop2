import csv
import argparse
from apyori import apriori
from sklearn import model_selection

class RecommendationManager():
    def __init__(self,
                 data_file_name,
                 random_state: float=1,
                 test_size: float=0.1,
                 min_support: float=0.0045,
                 min_confidence: float=0.2,
                 min_lift: float=1.5,):
        self.DATA = self.load_data(data_file_name)
        self.TRAINING_DATA,self.TESTING_DATA = self.split_data(self.DATA,random_state,test_size)
        self.RELATIONS = self.find_relations(self.TRAINING_DATA, min_support, min_confidence, min_lift)
        self.RULE = self.make_recommendation_rules(self.RELATIONS)
    @staticmethod
    def load_data(data_file_name: str):
        data = []
        with open(data_file_name) as data_file:
            csv_reader = csv.reader(data_file,skipinitialspace =True)
            for row in csv_reader:
                data.append(row)
        return data
    @staticmethod
    def split_data(data: list,random_state:float,test_size:float)-> (list,list):
        train, test = model_selection.train_test_split(data,random_state = random_state, test_size=test_size)
        return (train,test)
    @staticmethod
    def find_relations(data:list,min_support: float,min_confidence: float,min_lift: float):
        result = list(apriori(data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift))
        return result
    @staticmethod
    def make_recommendation_rules(relations: list) -> list:
        rules = []
        for relation in relations:
            stat = relation.ordered_statistics[0]
            rules.append({
                'items': relation.items,
                'items_base': stat.items_base,
                'items_add' : stat.items_add,
                'lift' : stat.lift
            })
        return rules

    def get_recommendations(self,item_in_cart: list)->list:
        recommendations = []
        items = frozenset(item_in_cart)
        rules = self.RULE
        for rule in rules:
            if rule['items_base'] <= items:
            #if rule['item_base'].issubset(items):
                recommendations.append({
                'item':list(rule['items_add'])[0],
                'strength': rule['lift']
                }) 
        def sorter(r):
            return r['strength']
        return sorted(recommendations,key=sorter,reverse = True)

if __name__ == "__main__":
    recommendation_manager = RecommendationManager('store_data.csv')
    #print(recommendation_manager.DATA)
    print (recommendation_manager.get_recommendations(['chocolate','milk']))